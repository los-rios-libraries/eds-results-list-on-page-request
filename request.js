// goes in results list widget. Requires jQuery
$(function() {
  $('.result-list-li', window.parent.document).each(function() {
    var item = $(this);
    var rtac = item.find('.rtac');
    if (rtac.length) {
      var record = item.find('.rtac').data('rtac').RecordAN;
      if (record.indexOf('lrois') > -1) {
        var customLink = item.find('.icon-image-link:contains("Request")'); // if an additional kind of request link is used, this would need to be tweaked. Right now our ILL links are only on detailed records
        customLink.removeAttr('onclick'); // this needs to be removed, otherwise teh regular custom link behavior takes place. Not clear if this will effect stats in EBSCOadmin--seems likely
        customLink.on('click', function(e) {
          e.preventDefault();
          showRequestPage(record, item);
        });
      }
    }
  });
});

function showRequestPage(record, item) {
  $('.results-lois-request-area', window.parent.document).each(function() {
    var a = $(this);
    if (a.hasClass('hidden') === false) {
      a.slideUp().addClass('hidden'); // if any are visible, hide them. Prevents multiple from being open on single page. No use case for it
    }
  });
  record = record.replace('lrois_', '.');
  var requestURL = 'https://lasiii.losrios.edu/search~S9?/' + record + '/' + record + '/1,1,1,B/request~' + record;
  var existingEl = window.parent.document.getElementById('lois-request-' + record);
  if (existingEl === null) { // if multiple request links are made on a page, we just hide them -- simplest way. This "if" makes sure we're only creating it if it's not already
    var requestArea = window.parent.document.createElement('div');
    requestArea.id = 'lois-request-' + record;
    requestArea.setAttribute('class', 'results-lois-request-area');
    // markup could probably be improved...
    requestArea.innerHTML = '<div class="results-lois-req-container"><div class="results-lois-req-controls"><a class="new-win" href="' + requestURL + '" target="_blank">Open in a new window</a><button id="remove-req-' + record + '" class="button" aria-role="close" type="button">X</button></div><div class="request-overflow"><iframe class="request-lois-iframe" width="100%" height="860" src="' + requestURL + '"></iframe></div></div>';
    item.append(requestArea);
    window.parent.document.getElementById('remove-req-' + record).addEventListener('click', function() {
      requestArea.className += ' hidden';
    });
  } else { // if the div is already there, just remove "hidden" from the class and it will show.
    existingEl.className = existingEl.className.replace('hidden', '');
  }
}